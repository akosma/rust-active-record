use crate::any_property::AnyProperty;
use crate::any_property_map::AnyPropertyMap;
use chrono::{DateTime, Utc};
use std::fmt;

#[derive(Clone, Debug)]
pub struct Object {
    id: i32,
    properties: AnyPropertyMap,
    dirty: bool,
    table_name: String,
}

impl Object {
    pub fn create() -> Object {
        Object {
            id: -1,
            properties: AnyPropertyMap::create(),
            dirty: true,
            table_name: String::from("objects"),
        }
    }

    pub fn set_name(&mut self, name: &'static str) {
        self.properties.insert(AnyProperty::from_str("name", name));
        self.set_dirty();
    }

    pub fn get_name(&self) -> &str {
        return self.properties.get("name").unwrap().get_str().unwrap();
    }

    pub fn set_valid(&mut self, valid: bool) {
        self.properties
            .insert(AnyProperty::from_bool("valid", valid));
        self.set_dirty();
    }

    pub fn get_valid(&self) -> bool {
        return self.properties.get("valid").unwrap().get_bool().unwrap();
    }

    pub fn set_price(&mut self, price: f64) {
        self.properties
            .insert(AnyProperty::from_float("price", price));
        self.set_dirty();
    }

    pub fn get_price(&self) -> f64 {
        return self.properties.get("price").unwrap().get_float().unwrap();
    }
}

impl fmt::Display for Object {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} => {}", self.get_id(), self.properties)
    }
}

pub trait ActiveRecord {
    fn get_id(&self) -> i32;
    fn from_database(table_name: &str, id: i32) -> Self;
    fn save(&mut self);
    fn set_dirty(&mut self);
    fn is_dirty(&self) -> bool;
    fn set_created_date(&mut self);
    fn set_modified_date(&mut self);
}

impl ActiveRecord for Object {
    fn from_database(table_name: &str, id: i32) -> Object {
        println!("SELECT * FROM {} WHERE id = {}", table_name, id);
        Object {
            id: id,
            properties: AnyPropertyMap::create(),
            dirty: false,
            table_name: String::from(table_name),
        }
    }

    fn get_id(&self) -> i32 {
        self.id
    }

    fn save(&mut self) {
        if self.id == -1 {
            self.set_created_date();
            self.set_modified_date();
            println!(
                "INSERT INTO {} ({}) VALUES ({})",
                self.table_name,
                self.properties.keys().join(", "),
                self.properties.values().join(", ")
            );
            self.id = 123;
        } else if self.dirty {
            self.set_modified_date();
            println!(
                "UPDATE {} SET ({}) WHERE id = {}",
                self.table_name,
                self.properties.key_values().join(", "),
                self.id
            );
        }
        self.dirty = false;
    }

    fn set_dirty(&mut self) {
        self.dirty = true;
    }

    fn is_dirty(&self) -> bool {
        return self.dirty;
    }

    fn set_created_date(&mut self) {
        let now: DateTime<Utc> = Utc::now();
        self.properties
            .insert(AnyProperty::from_datetime("created_date", now));
    }

    fn set_modified_date(&mut self) {
        let now: DateTime<Utc> = Utc::now();
        self.properties
            .insert(AnyProperty::from_datetime("modified_date", now));
    }
}
