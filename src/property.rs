use std::cmp;

#[derive(Clone, Debug)]
pub struct Property<T> {
    pub name: String,
    pub value: T,
}

impl<T> Property<T> {
    pub fn from(name: &str, value: T) -> Property<T> {
        Property {
            name: String::from(name),
            value: value,
        }
    }
}

impl<T> std::cmp::Eq for Property<T> where T: std::cmp::PartialEq {}

impl<T> cmp::PartialEq for Property<T>
where
    T: cmp::PartialEq,
{
    fn eq(&self, other: &Property<T>) -> bool {
        return self.name == other.name && self.value == other.value;
    }
}
