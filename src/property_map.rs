use crate::property::Property;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct PropertyMap<T> {
    properties: HashMap<String, Property<T>>,
}

impl<T> PropertyMap<T> {
    pub fn create() -> PropertyMap<T> {
        PropertyMap {
            properties: HashMap::with_capacity(30 as usize),
        }
    }

    pub fn from(properties: Vec<Property<T>>) -> PropertyMap<T> {
        let mut props: HashMap<String, Property<T>> = HashMap::with_capacity(30 as usize);
        for property in properties {
            let name = property.name.clone();
            props.insert(name, property);
        }
        PropertyMap { properties: props }
    }

    pub fn insert(&mut self, prop: Property<T>) {
        self.properties.insert(prop.name.clone(), prop);
    }

    pub fn get(&self, key: &str) -> Option<&Property<T>> {
        if self.properties.contains_key(key) {
            return Some(&self.properties[key]);
        }
        None
    }

    pub fn len(&self) -> usize {
        return self.properties.len();
    }
}
