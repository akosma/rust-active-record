use crate::any_property::AnyProperty;
use itertools::Itertools;
use std::collections::HashMap;
use std::fmt;
use std::vec::Vec;

#[derive(Clone, Debug)]
pub struct AnyPropertyMap {
    properties: HashMap<String, AnyProperty>,
}

impl AnyPropertyMap {
    pub fn create() -> AnyPropertyMap {
        AnyPropertyMap {
            properties: HashMap::with_capacity(30 as usize),
        }
    }

    pub fn from(properties: Vec<AnyProperty>) -> AnyPropertyMap {
        let mut props: HashMap<String, AnyProperty> = HashMap::with_capacity(30 as usize);
        for property in properties {
            let name = property.get_name().clone();
            props.insert(name, property);
        }
        AnyPropertyMap { properties: props }
    }

    pub fn insert(&mut self, prop: AnyProperty) {
        self.properties.insert(prop.get_name().clone(), prop);
    }

    pub fn get(&self, key: &str) -> Option<&AnyProperty> {
        if self.properties.contains_key(key) {
            return Some(&self.properties[key]);
        }
        None
    }

    pub fn len(&self) -> usize {
        self.properties.len()
    }

    pub fn keys(&self) -> Vec<String> {
        self.properties.keys().sorted().map(|e| e.clone()).collect()
    }

    pub fn values(&self) -> Vec<String> {
        self.keys()
            .iter()
            .map(|key| format!("{}", self.get(key).unwrap().get_value()))
            .collect()
    }

    pub fn key_values(&self) -> Vec<String> {
        self.keys()
            .iter()
            .map(|key| format!("{}", self.get(key).unwrap()))
            .collect()
    }
}

impl fmt::Display for AnyPropertyMap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (key, value) in &(self.properties) {
            writeln!(f, "{} => {}", key, value)?;
        }
        return Ok(());
    }
}
