use crate::property::Property;
use chrono::{DateTime, Utc};
use std::fmt;

// Idea borrowed from
// https://www.simonewebdesign.it/rust-hashmap-insert-values-multiple-types/

#[derive(Clone, Debug)]
pub enum AnyProperty {
    Str(Property<&'static str>),
    Int(Property<i32>),
    Float(Property<f64>),
    Bool(Property<bool>),
    DateTime(Property<DateTime<Utc>>),
}

impl AnyProperty {
    pub fn from_int(key: &str, value: i32) -> AnyProperty {
        AnyProperty::Int(Property::from(key, value))
    }

    pub fn from_str(key: &str, value: &'static str) -> AnyProperty {
        AnyProperty::Str(Property::from(key, value))
    }

    pub fn from_float(key: &str, value: f64) -> AnyProperty {
        AnyProperty::Float(Property::from(key, value))
    }

    pub fn from_bool(key: &str, value: bool) -> AnyProperty {
        AnyProperty::Bool(Property::from(key, value))
    }

    pub fn from_datetime(key: &str, value: DateTime<Utc>) -> AnyProperty {
        AnyProperty::DateTime(Property::from(key, value))
    }

    pub fn get_str(&self) -> Option<&str> {
        return match self {
            AnyProperty::Str(prop) => Some(&prop.value),
            _ => None,
        };
    }

    pub fn get_int(&self) -> Option<i32> {
        return match self {
            AnyProperty::Int(prop) => Some(prop.value),
            _ => None,
        };
    }

    pub fn get_float(&self) -> Option<f64> {
        return match self {
            AnyProperty::Float(prop) => Some(prop.value),
            _ => None,
        };
    }

    pub fn get_bool(&self) -> Option<bool> {
        return match self {
            AnyProperty::Bool(prop) => Some(prop.value),
            _ => None,
        };
    }

    pub fn get_datetime(&self) -> Option<DateTime<Utc>> {
        return match self {
            AnyProperty::DateTime(prop) => Some(prop.value),
            _ => None,
        };
    }

    pub fn get_name(&self) -> &String {
        return match self {
            AnyProperty::Str(prop) => &prop.name,
            AnyProperty::Int(prop) => &prop.name,
            AnyProperty::Float(prop) => &prop.name,
            AnyProperty::Bool(prop) => &prop.name,
            AnyProperty::DateTime(prop) => &prop.name,
        };
    }

    pub fn get_value(&self) -> String {
        return match self {
            AnyProperty::Str(prop) => format!("'{}'", prop.value),
            AnyProperty::Int(prop) => format!("{}", prop.value),
            AnyProperty::Float(prop) => format!("{}", prop.value),
            AnyProperty::Bool(prop) => {
                let val = if prop.value { "TRUE" } else { "FALSE" };
                format!("{}", val)
            }
            AnyProperty::DateTime(prop) => format!("'{}'", prop.value.to_rfc3339()),
        };
    }
}

impl fmt::Display for AnyProperty {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} = {}", self.get_name(), self.get_value())
    }
}
