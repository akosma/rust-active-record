pub mod any_property;
pub mod any_property_map;
pub mod object;
pub mod property;
pub mod property_map;

#[cfg(test)]
mod tests {
    use crate::any_property::AnyProperty;
    use crate::any_property_map::AnyPropertyMap;
    use crate::object::ActiveRecord;
    use crate::object::Object;
    use crate::property::Property;
    use crate::property_map::PropertyMap;
    use chrono::{DateTime, NaiveDateTime, Utc};

    #[test]
    fn create_int_property() {
        let a = Property::from("key", 22);
        assert_eq!(a.name, "key");
        assert_eq!(a.value, 22);
    }

    #[test]
    fn create_str_property() {
        let a = Property::from("key", "value");
        assert_eq!(a.name, "key");
        assert_eq!(a.value, "value");
    }

    #[test]
    fn create_float_property() {
        let a = Property::from("key", 1.234);
        assert_eq!(a.name, "key");
        assert_eq!(a.value, 1.234);
    }

    #[test]
    fn create_bool_property() {
        let a = Property::from("key", true);
        assert_eq!(a.name, "key");
        assert_eq!(a.value, true);
    }

    #[test]
    fn create_datetime_property() {
        let now = Utc::now();
        let a: Property<DateTime<Utc>> = Property::from("key", now.clone());
        assert_eq!(a.name, "key");
        assert!(a.value == now);
    }

    #[test]
    fn property_cloning() {
        let a = Property::from("age", 22);
        let b = a.clone();
        assert_eq!(a, b);
    }

    #[test]
    fn property_equality() {
        let a = Property::from("age", 22);
        let b = Property::from("age", 22);
        assert!(a == b);

        let c = Property::from("test", 45);
        let d = Property::from("another", 45);
        assert!(c != d);

        let e = Property::from("test", 4578);
        let f = Property::from("another", 45);
        assert!(e != f);
    }

    #[test]
    fn property_map_create() {
        let a = Property::from("age", 22);
        let b = a.clone();
        let c = Property::from("age", 256);
        let d = Property::from("another", 22);
        let map: PropertyMap<i32> = PropertyMap::from(vec![a, b, c, d]);
        // a, b, and c have the same property
        // hence only the last one remains
        assert_eq!(map.len(), 2);
        if let Some(prop) = map.get("age") {
            assert!(true);
            assert_eq!(prop.value, 256);
        } else {
            assert!(false);
        }
    }

    #[test]
    fn property_map_get() {
        let a = Property::from("key", 22);
        let map: PropertyMap<i32> = PropertyMap::from(vec![a]);
        if let Some(prop) = map.get("key") {
            assert!(true);
            assert_eq!(prop.value, 22);
        } else {
            assert!(false);
        }
    }

    #[test]
    fn create_any_property() {
        let a = Property::from("key", 22);
        let b = AnyProperty::Int(a);
        if let Some(value) = b.get_int() {
            assert_eq!(value, 22);
        } else {
            assert!(false);
        }
    }

    #[test]
    fn any_property_clone() {
        let a = Property::from("key", 22);
        let b = AnyProperty::Int(a);
        let c = b.clone();
        if let Some(value) = c.get_int() {
            assert_eq!(value, 22);
        } else {
            assert!(false);
        }
    }

    #[test]
    fn create_any_property_map() {
        let a = AnyProperty::from_int("int", 22);
        let b = AnyProperty::from_str("str", "value");
        let c = AnyProperty::from_float("float", 1.234);
        let d = AnyProperty::from_bool("bool", true);
        let e = AnyProperty::from_datetime("date", Utc::now());
        let m = AnyPropertyMap::from(vec![a, b, c, d, e]);
        assert_eq!(m.len(), 5);
    }

    #[test]
    fn any_property_map_get() {
        let a = AnyProperty::from_int("int", 22);
        let b = AnyProperty::from_str("str", "value");
        let c = AnyProperty::from_float("float", 1.234);
        let d = AnyProperty::from_bool("bool", true);
        let e = AnyProperty::from_datetime("date", Utc::now());
        let m = AnyPropertyMap::from(vec![a, b, c, d, e]);
        if let Some(prop) = m.get("str") {
            let s = format!("{}", prop);
            assert_eq!(s, "str = 'value'");
        } else {
            assert!(false);
        }
        if let Some(prop) = m.get("bool") {
            let s = format!("{}", prop);
            assert_eq!(s, "bool = TRUE");
        } else {
            assert!(false);
        }
    }

    #[test]
    fn any_property_map_insert() {
        let a = AnyProperty::from_int("int", 22);
        let b = AnyProperty::from_str("str", "value");
        let c = AnyProperty::from_float("float", 1.234);
        let d = AnyProperty::from_bool("bool", true);
        let e = AnyProperty::from_datetime("date", Utc::now());
        let mut m = AnyPropertyMap::create();
        m.insert(a);
        m.insert(b);
        m.insert(c);
        m.insert(d);
        m.insert(e);
        assert_eq!(m.len(), 5);
        if let Some(prop) = m.get("str") {
            let s = format!("{}", prop);
            assert_eq!(s, "str = 'value'");
        } else {
            assert!(false);
        }
        if let Some(prop) = m.get("bool") {
            let s = format!("{}", prop);
            assert_eq!(s, "bool = TRUE");
        } else {
            assert!(false);
        }
    }

    #[test]
    fn any_property_map_queries() {
        let a = AnyProperty::from_int("int", 22);
        let b = AnyProperty::from_str("str", "value");
        let c = AnyProperty::from_float("float", 1.234);
        let d = AnyProperty::from_bool("bool", true);
        let e = AnyProperty::from_datetime(
            "date",
            DateTime::from_utc(NaiveDateTime::from_timestamp(61, 0), Utc),
        );
        let m = AnyPropertyMap::from(vec![a, b, c, d, e]);
        let keys = m.keys();
        assert_eq!(keys, vec!["bool", "date", "float", "int", "str"]);
        let values = m.values();
        assert_eq!(
            values,
            vec![
                "TRUE",
                "'1970-01-01T00:01:01+00:00'",
                "1.234",
                "22",
                "'value'",
            ]
        );
        let key_values = m.key_values();
        assert_eq!(
            key_values,
            vec![
                "bool = TRUE",
                "date = '1970-01-01T00:01:01+00:00'",
                "float = 1.234",
                "int = 22",
                "str = 'value'"
            ]
        );
    }

    #[test]
    fn active_record() {
        let mut o = Object::create();
        assert!(o.is_dirty());
        o.set_name("roger");
        assert!(o.is_dirty());
        assert_eq!(o.get_name(), "roger");
        o.save();
        assert!(!o.is_dirty());
    }
}
